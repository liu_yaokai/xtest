cmake_minimum_required(VERSION 3.25)
project(xtest C)

set(CMAKE_C_STANDARD 23)
add_compile_options("-fno-builtin")
