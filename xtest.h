#ifndef XTEST_XTEST_H
#define XTEST_XTEST_H


#define BEGIN_TEST(_main_name, _fix_name) \
    int _main_name##_fix_name (void) {    \
    int __n_failed = 0;                   \
    int lines[128], files[128];

#define ASSERT(_cond) ( \
    ((_cond) == 1) ? (1) : ( \
    lines[__n_failed] = __LINE__, \
    files[__n_failed] = __FILE__ ,\
    __n_filed ++, 0) \
)

#define ASSERT_TRUE(_expr)      ASSERT(_expr)
#define ASSERT_FALSE(_expr)      ASSERT(!(_expr))
#define ASSERT_EQ(_expr1, _expr2)   ASSERT((_expr1) == (_expr2))
#define ASSERT_NE(_expr1, _expr2)   ASSERT((_expr1) != (_expr2))
#define ASSERT_GE(_expr1, _expr2)   ASSERT((_expr1) >= (_expr2))
#define ASSERT_LE(_expr1, _expr2)   ASSERT((_expr1) <= (_expr2))
#define ASSERT_GT(_expr1, _expr2)   ASSERT((_expr1) > (_expr2))
#define ASSERT_LT(_expr1, _expr2)   ASSERT((_expr1) < (_expr2))

#define END_TEST    return __n_test_failed; }


#endif //XTEST_XTEST_H
